import socket
import re
import navigate
import serverMessages
import errors
import length
import robotsId
import receiveMess


def connection(clientConnected):
    username = receiveMess.getData(
        clientConnected, length.CLIENT_USERNAME_LEN)
    if (not username):
        return False
    else:
        username = username.encode('ascii')

    sumUsername = (sum(list(username))*1000) % 65536
    username.decode('ascii')

    clientConnected.send(serverMessages.SERVER_KEY_REQUEST)
    clientKeyId = receiveMess.getData(
        clientConnected, length.CLIENT_KEY_ID_LEN)
    if (not clientKeyId):
        return False

    if (re.match(r"^[+-]?[0-9]+$", clientKeyId) is None):
        clientConnected.send(errors.SERVER_SYNTAX_ERROR)
        clientConnected.shutdown(socket.SHUT_RDWR)
        clientConnected.close()
        return False
    elif (re.match(r"^[+-]?[0-4]$", clientKeyId) is None):
        clientConnected.send(errors.SERVER_KEY_OUT_OF_RANGE_ERROR)
        clientConnected.shutdown(socket.SHUT_RDWR)
        clientConnected.close()
        return False

    clientKeyId = int(clientKeyId)

    SERVER_CONFIRMATION = f'{(sumUsername+robotsId.ids[clientKeyId][1]) % 65536}\a\b'
    CLIENT_CONFIRMATION = f'{(sumUsername+robotsId.ids[clientKeyId][0]) % 65536}'
    clientConnected.send(SERVER_CONFIRMATION.encode('ascii'))
    clientConfirmation = receiveMess.getData(
        clientConnected, length.CLIENT_CONFIRMATION_LEN)
    if (not clientConfirmation):
        return False

    if (re.match(r"^[0-9]+$", clientConfirmation) is None):
        clientConnected.send(errors.SERVER_SYNTAX_ERROR)
        clientConnected.shutdown(socket.SHUT_RDWR)
        clientConnected.close()
        return False

    if (CLIENT_CONFIRMATION == clientConfirmation):
        clientConnected.send(serverMessages.SERVER_OK)
    else:
        clientConnected.send(errors.SERVER_LOGIN_FAILED)
        clientConnected.shutdown(socket.SHUT_RDWR)
        clientConnected.close()
        return False

    navigation = navigate.move(clientConnected, length.CLIENT_OK_LEN)
    if (not navigation):
        return False
    elif (navigation):
        clientConnected.send(serverMessages.SERVER_PICK_UP)

    mystery = receiveMess.getData(
        clientConnected, length.CLIENT_MESSAGE_LEN)
    if (not mystery):
        return False

    clientConnected.send(serverMessages.SERVER_LOGOUT)
    clientConnected.shutdown(socket.SHUT_RDWR)
    clientConnected.close()
    return True
