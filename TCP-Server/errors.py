SERVER_LOGIN_FAILED = '300 LOGIN FAILED\a\b'.encode(
    'ascii')
SERVER_SYNTAX_ERROR = '301 SYNTAX ERROR\a\b'.encode(
    'ascii')
SERVER_KEY_OUT_OF_RANGE_ERROR = '303 KEY OUT OF RANGE\a\b'.encode('ascii')
