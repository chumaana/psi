import oneMove
import serverMessages
import obstacle
import math


def move(clientConnected, length):
    position = []
    mes = oneMove.oneMove(clientConnected, length, serverMessages.SERVER_MOVE)
    if (not mes):
        return False

    mes = mes.split()
    prev = (int(mes[1]), int(mes[2]))
    if (prev[0] == 0 and prev[1] == 0):
        return True
    mes = oneMove.oneMove(clientConnected, length, serverMessages.SERVER_MOVE)
    if (not mes):
        return False

    mes = mes.split()
    position = (int(mes[1]), int(mes[2]))
    x = position[0]
    y = position[1]

    while (x != 0 or y != 0):
        if (prev[0] == x and prev[1] == y):
            mes = obstacle.obstacleAvoidance(clientConnected, length)
            if (not mes):
                return False

            mes = mes.split()
            x = int(mes[1])
            y = int(mes[2])
            continue

        if (math.dist([x, y], [0, 0]) > math.dist(prev, [0, 0])):
            mes = oneMove.oneMove(clientConnected,
                                  length, serverMessages.SERVER_TURN_RIGHT)
            if (not mes):
                return False
            mes = oneMove.oneMove(clientConnected,
                                  length, serverMessages.SERVER_TURN_RIGHT)
            if (not mes):
                return False

            mes = mes.split()
            x = int(mes[1])
            y = int(mes[2])

        if ((x == 0 and y < 0 and prev[0] > 0) or (x == 0 and y > 0 and prev[0] < 0) or (y == 0 and x > 0 and prev[1] > 0) or (y == 0 and x < 0 and prev[1] < 0)):
            mes = oneMove.oneMove(clientConnected,
                                  length, serverMessages.SERVER_TURN_RIGHT)
            if (not mes):
                return False

            prev = (x, y)
            mes = mes.split()
            x = int(mes[1])
            y = int(mes[2])
        elif ((x == 0 and y < 0 and prev[0] < 0) or (x == 0 and y > 0 and prev[0] > 0) or (y == 0 and x > 0 and prev[1] < 0) or (y == 0 and x < 0 and prev[1] > 0)):
            mes = oneMove.oneMove(clientConnected,
                                  length, serverMessages.SERVER_TURN_LEFT)
            if (not mes):
                return False

            prev = (x, y)
            mes = mes.split()
            x = int(mes[1])
            y = int(mes[2])

        mes = oneMove.oneMove(clientConnected,
                              length, serverMessages.SERVER_MOVE)
        if (not mes):
            return False

        prev = (x, y)
        mes = mes.split()
        x = int(mes[1])
        y = int(mes[2])

    return True
