import socket
import errors

all = ''


def getData(clientConnected, length):
    global all
    buff = ''
    while True:
        if '\a\b' not in all:
            clientConnected.settimeout(1)
            try:
                all += clientConnected.recv(8).decode('ascii')
            except TimeoutError:
                clientConnected.shutdown(socket.SHUT_RDWR)
                clientConnected.close()
                return False
            else:
                clientConnected.settimeout(None)
        if (not all):
            clientConnected.shutdown(socket.SHUT_RDWR)
            clientConnected.close()
            return False

        if (all.find('\a\b') == -1 and (len(all) >= length-2)):
            clientConnected.send(errors.SERVER_SYNTAX_ERROR)
            clientConnected.shutdown(socket.SHUT_RDWR)
            clientConnected.close()
            return False

        if (all.find('\a\b') == -1):
            continue
        elif (len(all.split('\a\b')[0]) > length-2):
            clientConnected.send(errors.SERVER_SYNTAX_ERROR)
            clientConnected.shutdown(socket.SHUT_RDWR)
            clientConnected.close()
            return False

        else:
            buff = all.split('\a\b')[0]
            all = all[all.find('\a\b')+2:]
            return buff
