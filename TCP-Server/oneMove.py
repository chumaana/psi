import socket
import errors
import receiveMess
import re


def oneMove(clientConnected, length, serverMes):

    clientConnected.send(serverMes)
    one = receiveMess.getData(clientConnected, length)
    if (not one):
        return False

    if (re.match(r"^OK [+-]?[0-9]+ [+-]?[0-9]+$", one) is None):
        clientConnected.send(errors.SERVER_SYNTAX_ERROR)
        clientConnected.shutdown(socket.SHUT_RDWR)
        clientConnected.close()
        return False
    else:
        return one
