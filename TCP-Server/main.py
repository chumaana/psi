import socket
import connect
import receiveMess


def main():

    serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    port = 8050
    serverAddress = "localhost"
    print(f'starting up on {serverAddress} port {port}')
    serverSocket.bind((serverAddress, port))
    serverSocket.listen(5)

    while True:
        (clientConnected, clientAddress) = serverSocket.accept()
        print(f'connected client {clientAddress}')
        try:
            connect.connection(clientConnected)
        except Exception as ex:
            print(ex)
        receiveMess.all = ''


if __name__ == "__main__":
    main()
