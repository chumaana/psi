SERVER_MOVE = '102 MOVE\a\b'.encode('ascii')
SERVER_TURN_LEFT = '103 TURN LEFT\a\b'.encode(
    'ascii')
SERVER_TURN_RIGHT = '104 TURN RIGHT\a\b'.encode(
    'ascii')
SERVER_PICK_UP = '105 GET MESSAGE\a\b'.encode(
    'ascii')
SERVER_LOGOUT = '106 LOGOUT\a\b'.encode('ascii')
SERVER_OK = '200 OK\a\b'.encode('ascii')
SERVER_KEY_REQUEST = '107 KEY REQUEST\a\b'.encode('ascii')
