import oneMove
import serverMessages


def obstacleAvoidance(clientConnected, length):
    one = oneMove.oneMove(clientConnected, length,
                          serverMessages.SERVER_TURN_RIGHT)
    if (not one):
        return False
    one = oneMove.oneMove(clientConnected, length, serverMessages.SERVER_MOVE)
    if (not one):
        return False
    mes = one.split()
    if (mes[1] == '0' or mes[2] == '0'):
        return one
    one = oneMove.oneMove(clientConnected, length,
                          serverMessages.SERVER_TURN_LEFT)
    if (not one):
        return False
    one = oneMove.oneMove(clientConnected, length, serverMessages.SERVER_MOVE)
    if (not one):
        return False
    mes = one.split()
    if (mes[1] == '0' or mes[2] == '0'):
        return one
    one = oneMove.oneMove(clientConnected, length, serverMessages.SERVER_MOVE)
    if (not one):
        return False
    mes = one.split()
    if (mes[1] == '0' or mes[2] == '0'):
        return one
    one = oneMove.oneMove(clientConnected, length,
                          serverMessages.SERVER_TURN_LEFT)
    if (not one):
        return False
    one = oneMove.oneMove(clientConnected, length, serverMessages.SERVER_MOVE)
    if (not one):
        return False
    mes = one.split()
    if (mes[1] == 0 or mes[2] == 0):
        return one
    one = oneMove.oneMove(clientConnected, length,
                          serverMessages.SERVER_TURN_RIGHT)
    if (not one):
        return False
    return one
